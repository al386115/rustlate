mod translator;
pub use translator::*;

/// Translates the text with shotcut.
///
/// # Arguments
///
/// * `text` - The string slice text will be translated.
/// * `to` - The string slice language will be converted to.
///
/// # Examples
///
/// ```
/// fn main() {
///     println!("{:?}", rustlate::translate_auto("hello", "tr"));
/// }
/// ```
pub fn translate_auto(text: &str, to: &'static str) -> Result<String, String> {
    let translator_struct = Translator{
        to: to,
        from: "auto"
    };

    translator_struct.translate(text)
}