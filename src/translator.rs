use reqwest;
use serde_json::{self, Value};
use std::process::exit;
/// Translator struct.
///
/// # Arguments
///
/// * `to` - The static string slice text will be translated to.
/// * `from` - The static string slice text will be translated from.
///
/// # Examples
///
/// ```
/// fn main() {
///     let translator_struct = rustlate::Translator{
///         to: "en",
///         from: "tr"
///     };
/// }
/// ```
/// 
#[derive(Clone,Copy)]
pub struct Translator<'a>{
    pub to: &'a str,
    pub from: &'a str
}
impl<'a> Translator<'a> {
    /// Translates the text.
    ///
    /// # Arguments
    ///
    /// * `text` - The string slice text will be translated.
    ///
    /// # Examples
    ///
    /// ```
    /// fn main() {
    ///     let translator_struct = rustlate::Translator{
    ///         to: "tr",
    ///         from: "en"
    ///     };
    ///
    ///     println!("{:?}", translator_struct.translate("cat"));
    /// }
    /// ```
    pub fn translate(&self, text: & str) -> Result<String, String> {
        if text == ""{
            return Ok(text.to_string())
        }
        parse_result(fetch_page(text, self.from, self.to))
    }
}

fn fetch_page(text: &str, from: &str, to: &str) -> Vec<Value> {
    let formatted_url = format!("https://translate.googleapis.com/translate_a/single?client=gtx&ie=UTF-8&oe=UTF-8&dt=t&tl={}&sl={}&q={}", to, from, text);

    let v = reqwest::blocking::get(&formatted_url)
        .and_then(|resp| resp.text())
        .and_then(|body| Ok(serde_json::from_str::<Vec<Value>>(&body)))
        .unwrap_or_else(|_| {
            eprintln!("{}", "Network error...");
            exit(1);
        })
        .unwrap_or_else(|_| {
            eprintln!("{}", "JSON parse error...");
            exit(1);
        });
    v
}

fn parse_result(result: Vec<Value>) -> Result<String, String> {
    match result.first() {
        Some(item) => {
            let result = item
                .as_array()
                .unwrap()
                .iter()
                .map(|s| s[0].as_str().unwrap())
                .collect::<Vec<&str>>()
                .join(" ");
            return Ok(result);
        }
        None => {eprintln!("{}", "Error...");
                exit(-1);},
    }
}
